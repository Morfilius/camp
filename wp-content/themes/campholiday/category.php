<?php
get_header();?>
    <main class="content">
        <div class="container">
            <?php breadcrumbs()?>
        </div>
        <section class="articles-sect">
            <div class="container">
                <h1 class="title">Блог</h1>
	            <?php if (have_posts()) : ?>
                    <div class="row">
			            <?while (have_posts()) : the_post();?>
				            <?php get_template_part('parts/loop/blog');?>
			            <?php endwhile;?>
                    </div>
	            <?else: echo '<p>Нет записей.</p>';?>
	            <? endif;?>
            </div>
        </section>


        <?php echo pagination()?>
    </main>
<?php get_footer();?>