<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
require_once( __DIR__.'/php/custom-types.php');
//require_once( __DIR__.'/php/shortcodes.php');
require_once( __DIR__.'/php/widgets.php');


add_theme_support('title-tag');
add_theme_support( 'woocommerce' );


register_nav_menus(array(
	'top' => 'Верхнее',
	'main' => 'Основное',
	'footer-1' => 'Подвал 1',
	'footer-2' => 'Подвал 2',
	'footer-3' => 'Подвал 3',
	'bottom' => 'Внизу'
));

add_theme_support('post-thumbnails');
set_post_thumbnail_size(250, 150);
add_image_size('big-prod-thumb', 726, 515, false);
add_image_size('prod-thumb-mob', 450, 302, false);
add_image_size('prod-thumb', 268, 163, false);
add_image_size('prod-camp', 145, 151, false);
add_image_size('prod-info', 318, 206, false);
add_image_size('blog-loop', 541, 276, false);
add_image_size('related-thumb', 301, 160, false);
add_image_size('home-slider', 779, 369, false);
add_image_size('home-slider-thumb', 192, 98, false);

register_sidebar(array( // регистрируем левую колонку, этот кусок можно повторять для добавления новых областей для виджитов
	'name' => 'Сайдбар', // Название в админке
	'id' => "sidebar", // идентификатор для вызова в шаблонах
	'description' => 'Обычная колонка в сайдбаре', // Описалово в админке
	'before_widget' => '<div id="%1$s" class="widget %2$s">', // разметка до вывода каждого виджета
	'after_widget' => "</div>\n", // разметка после вывода каждого виджета
	'before_title' => '<span class="widgettitle">', //  разметка до вывода заголовка виджета
	'after_title' => "</span>\n", //  разметка после вывода заголовка виджета
));

register_sidebar(array(
	'name' => 'Футер - 1',
	'id' => "footer-1",
	'description' => 'Колонка в подвале',
	'before_widget' => '',
	'after_widget' => "",
	'before_title' => '',
	'after_title' => "",
));

register_sidebar(array(
	'name' => 'Футер - 2',
	'id' => "footer-2",
	'description' => 'Колонка в подвале',
	'before_widget' => '',
	'after_widget' => "",
	'before_title' => '<span class="company__title">',
	'after_title' => "</span>",
));

register_sidebar(array(
	'name' => 'Футер - 3',
	'id' => "footer-3",
	'description' => 'Колонка в подвале',
	'before_widget' => '',
	'after_widget' => "",
	'before_title' => '',
	'after_title' => "",
));

if (!class_exists('clean_comments_constructor')) { // если класс уже есть в дочерней теме - нам не надо его определять
	class clean_comments_constructor extends Walker_Comment { // класс, который собирает всю структуру комментов
		public function start_lvl( &$output, $depth = 0, $args = array()) { // что выводим перед дочерними комментариями
			$output .= '<ul class="children">' . "\n";
		}
		public function end_lvl( &$output, $depth = 0, $args = array()) { // что выводим после дочерних комментариев
			$output .= "</ul><!-- .children -->\n";
		}
	    protected function comment( $comment, $depth, $args ) { // разметка каждого комментария, без закрывающего </li>!
	    	$classes = implode(' ', get_comment_class()).($comment->comment_author_email == get_the_author_meta('email') ? ' author-comment' : ''); // берем стандартные классы комментария и если коммент пренадлежит автору поста добавляем класс author-comment
	        echo '<li id="comment-'.get_comment_ID().'" class="'.$classes.' media">'."\n"; // родительский тэг комментария с классами выше и уникальным якорным id
	    	echo '<div class="media-left">'.get_avatar($comment, 64, '', get_comment_author(), array('class' => 'media-object'))."</div>\n"; // покажем аватар с размером 64х64
	    	echo '<div class="media-body">';
	    	echo '<span class="meta media-heading">Автор: '.get_comment_author()."\n"; // имя автора коммента
	    	//echo ' '.get_comment_author_email(); // email автора коммента, плохой тон выводить почту
	    	echo ' '.get_comment_author_url(); // url автора коммента
	    	echo ' Добавлено '.get_comment_date('F j, Y в H:i')."\n"; // дата и время комментирования
	    	if ( '0' == $comment->comment_approved ) echo '<br><em class="comment-awaiting-moderation">Ваш комментарий будет опубликован после проверки модератором.</em>'."\n"; // если комментарий должен пройти проверку
	    	echo "</span>";
	        comment_text()."\n"; // текст коммента
	        $reply_link_args = array( // опции ссылки "ответить"
	        	'depth' => $depth, // текущая вложенность
	        	'reply_text' => 'Ответить', // текст
				'login_text' => 'Вы должны быть залогинены' // текст если юзер должен залогинеться
	        );
	        echo get_comment_reply_link(array_merge($args, $reply_link_args)); // выводим ссылку ответить
	        echo '</div>'."\n"; // закрываем див
	    }
	    public function end_el( &$output, $comment, $depth = 0, $args = array() ) { // конец каждого коммента
			$output .= "</li><!-- #comment-## -->\n";
		}
	}
}

if (!function_exists('pagination')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function pagination($flag = false) { // функция вывода пагинации
		global $wp_query; // текущая выборка должна быть глобальной
		$big = 999999999; // число для замены
		$links = paginate_links(array( // вывод пагинации с опциями ниже
			'prev_next' => true,
			'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))), // что заменяем в формате ниже
			'format' => '?paged=%#%', // формат, %#% будет заменено
			'current' => max(1, get_query_var('paged')), // текущая страница, 1, если $_GET['page'] не определено
			'type' => 'array', // нам надо получить массив
			'prev_text'    => '<', // текст назад
			'next_text'    => '>', // текст вперед
			'total' => $wp_query->max_num_pages, // общие кол-во страниц в пагинации
			'show_all'     => false, // не показывать ссылки на все страницы, иначе end_size и mid_size будут проигнорированны
			'end_size'     => 2, //  сколько страниц показать в начале и конце списка (12 ... 4 ... 89)
			'mid_size'     => 2, // сколько страниц показать вокруг текущей страницы (... 123 5 678 ...).
			'add_args'     => false, // массив GET параметров для добавления в ссылку страницы
			'add_fragment' => '',	// строка для добавления в конец ссылки на страницу
			'before_page_number' => '', // строка перед цифрой
			'after_page_number' => '' // строка после цифры
		));
		if( is_array( $links ) ) { // если пагинация есть
			if($flag) return $flag;
			echo '<div class="pagination">','<div class="container">','<ul class="pagination__list">';
			foreach ( $links as $link ) {
				$link = str_replace('page/1/','',$link);
				$link = str_replace('page-numbers dots','pagination__item',$link);
				if ( strpos( $link, 'current' ) !== false ) echo "<li class='breadcrumbs-pagination__list-item active'>".get_regex_link($link,'active')."</li>"; // если это активная страница
				elseif(strpos( $link, 'next' ) !== false) echo str_replace('page-numbers','pagination__item pagination__item_last',$link);
				elseif(strpos( $link, 'prev' ) !== false) echo str_replace('page-numbers','pagination__item pagination__item_last',$link);
				else echo "<li class='breadcrumbs-pagination__link'>".get_regex_link($link)."</li>";
			}
			echo '</ul>','</div>','</div>';
		}
	}
}

// вспомогательная для пагинации возвращает ссылки и нумерацию
function get_regex_link($link,$flag = false){
	if($flag === 'active'){
		$pattern = '/>([\d]+)</';
		$format = '<span class="pagination__item active">%1$d</span>';
	}else{
		$pattern = '/href=[\'\"](.*)[\'\"]>([\d]+)</';
		$format = '<a class="pagination__item" href="%1$s">%2$d</a>';
	}
	if(preg_match($pattern,$link,$match))
		if(count($match) == 2){
			return sprintf($format, $match[1]);
		}elseif (count($match) == 3){
			return sprintf($format, $match[1],$match[2]);
		}
	return $link;
}

add_action('wp_enqueue_scripts', 'add_scripts');
if (!function_exists('add_scripts')) {
	function add_scripts() {
	    if(is_admin()) return false;
	    //wp_deregister_script('jquery');
	    wp_enqueue_script('libs', get_template_directory_uri().'/js/libs.js','','',true);
	    wp_enqueue_script('main', get_template_directory_uri().'/js/main.js','','',true);
	    wp_enqueue_script('main-add', get_template_directory_uri().'/js/common.js','','',true);
		if($GLOBALS['wp_query']->queried_object_id === 20395){
			wp_enqueue_script('wc-booking', plugins_url().'/woocommerce/assets/js/frontend/add-to-cart-variation.min.js',array( 'jquery', 'wp-util' ),'',true);
		}
	}
}

add_action('wp_enqueue_scripts', 'add_styles');
if (!function_exists('add_styles')) {
	function add_styles() {
	    if(is_admin()) return false;
		wp_enqueue_style( 'libs', get_template_directory_uri().'/css/libs.min.css' );
		wp_enqueue_style( 'main', get_template_directory_uri().'/css/style.css' );
		wp_enqueue_style( 'style', get_template_directory_uri().'/style.css' );
	}
}


//подключении страниц с опциями acf
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Настройки темы',
		'menu_title'	=> 'Настройки темы',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'position'      => 59,
		'redirect'		=> false
	));
//	acf_add_options_sub_page(array(
//		'page_title' 	=> 'Доктора',
//		'menu_title'	=> 'Доктора',
//		'menu_slug' 	=> 'doctors',
//		'parent_slug'	=> 'seo-settings',
//	));
}

//ХУКИ
add_filter('nav_menu_css_class','change_top_menu_class_li',20,4);
function change_top_menu_class_li($classes, $item, $args, $depth){
//	if($args->theme_location != 'top') return $classes;
	return str_replace(array(
		'current-menu-item',
		'current-menu-parent',
	),'active',$classes);
}

//add_filter('nav_menu_link_attributes','change_top_menu_class_a',20,4);
//function change_top_menu_class_a($atts, $item, $args, $depth){
//	if($args->theme_location != 'top') return $atts;
//	$atts['class'] = 'main-nav__link';
//	return $atts;
//}

function is_for($array){
	return (is_array($array) && !empty($array));
}

//ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ
//обработка телефона для ссылки tel:
function get_tel($phone){
	return 'tel:8'.str_replace([' ','(',')','-'],'',$phone);
}

function get_terms_post($product_id,$term){
	return get_terms(array(
		'taxonomy'   => $term,
		'object_ids' => $product_id
	));
}



//доп шорткод в cf7
add_action( 'wpcf7_init', 'custom_add_form_tag_policy' );
function custom_add_form_tag_policy() {
	wpcf7_add_form_tag( 'policy', 'custom_policy_form_tag_handler',array( 'name-attr' => true ));
}

function custom_policy_form_tag_handler( $tag ) {
	return '<span class="wpcf7-form-control-wrap policy">
        <label>
            <input type="checkbox" class="checkbox" name="'.$tag->name.'"
                   value="1" checked="checked">
            <span class="callback-form__policy-text checkbox-text">
                <span class="checkbox-img"></span>
                Выражаю свое согласие на обработку
                моих персональных данных, в соответствии с
                Федеральным законом «О персональных данных» от
                27.07.2006 г. No 152­ФЗ."
            </span>
        </label></span>';
}

add_action( 'wpcf7_init', 'custom_add_form_tag_rating' );
function custom_add_form_tag_rating() {
	wpcf7_add_form_tag( 'rating', 'custom_rating_form_tag_handler',array( 'name-attr' => true ));
}

function custom_rating_form_tag_handler( $tag ) {
	return '
           <td class="ratings__stars ratings__td">
           <span class="wpcf7-form-control-wrap '.$tag->name.'">
             <span class="stars-form"></span>
           </span>
           </td>
        ';
}



//доп select
add_action( 'wpcf7_init', 'custom_add_form_tag_shift' );
function custom_add_form_tag_shift() {
	wpcf7_add_form_tag( 'shift', 'custom_shift_form_tag_handler',array( 'name-attr' => true ));
}

function custom_shift_form_tag_handler( $tag ) {
	$product = wc_get_product(1461);
	$options = $product->get_variation_attributes();

	ob_start();
	wc_dropdown_variation_attribute_options( array(
		'options'   => $options['pa_smeny'],
		'attribute' => 'pa_smeny',
		'name '     => $tag->name,
		'product'   => $product,
		'class' => 'select-js booking-form__select search__select'
	) );

	return ob_get_clean();
}

//support
add_action( 'wpcf7_init', 'custom_add_form_tag_support' );
function custom_add_form_tag_support() {
	wpcf7_add_form_tag( 'support', 'custom_support_form_tag_handler',array( 'name-attr' => true ));
}

function custom_support_form_tag_handler( $tag ) {
	$params = array(
		'wc_ajax_url'                      => WC_AJAX::get_endpoint( '%%endpoint%%' ),
		'i18n_no_matching_variations_text' => esc_attr__( 'Sorry, no products matched your selection. Please choose a different combination.', 'woocommerce' ),
		'i18n_make_a_selection_text'       => esc_attr__( 'Please select some product options before adding this product to your cart.', 'woocommerce' ),
		'i18n_unavailable_text'            => esc_attr__( 'Sorry, this product is unavailable. Please choose a different combination.', 'woocommerce' ),
	);

	$js_var = apply_filters( 'woocommerce_get_script_data', $params, 'wc-add-to-cart-variation' );

	$html = '<script>var wc_add_to_cart_variation_params = '.json_encode($js_var).'</script>';
	$html .= '<script type="text/template" id="tmpl-variation-template">
                  <div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
                  <div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
              </script>';

	return $html;
}





//валидация custom cf7 шорткода
add_filter( 'wpcf7_validate_policy', 'custom_policy_confirmation_validation_filter', 20, 2 );
function custom_policy_confirmation_validation_filter( $result, $tag ) {

	if(!isset($_POST['policy']) && $_POST['policy'] !== 1){
		$result->invalidate( $tag, "Вы не согласились на обработку данных" );
	}

	return $result;
}

//валидация custom cf7 шорткода
add_filter( 'wpcf7_validate_rating', 'custom_rating_confirmation_validation_filter', 20, 2 );
function custom_rating_confirmation_validation_filter( $result, $tag ) {

    if(empty($_POST[$tag->name]))
	    $result->invalidate( $tag, "Пожалуйста, оцените." );

	return $result;
}

add_action( 'wpcf7_before_send_mail', 'add_custom_comment' );
function add_custom_comment($WPCF7_ContactForm)
{
	$wpcf7 = WPCF7_ContactForm :: get_current() ;
	$submission = WPCF7_Submission :: get_instance() ;
	if ($submission)
	{
		$posted_data = $submission->get_posted_data() ;
		if ( empty ($posted_data)) return ;


		$id = esc_html($posted_data['_wpcf7_container_post']);
		$tel = esc_html($posted_data['tel']);
		$fio = esc_html($posted_data['fio']);
		$adv = esc_html($posted_data['adv']);
		$noadv = esc_html($posted_data['noadv']);
		$prog = esc_html($posted_data['score0']);
		$food = esc_html($posted_data['score1']);
		$place = esc_html($posted_data['score2']);
		$infra = esc_html($posted_data['score3']);
		$price = esc_html($posted_data['score4']);


        $data = array(
            'comment_post_ID'      => $id,
            'comment_author'       => $fio,
            'comment_author_email' => '',
            'comment_author_url'   => '',
            'comment_content'      => '',
            'comment_type'         => '',
            'comment_parent'       => 0,
            'user_id'              => 0,
            'comment_date'         => current_time('mysql'),
            'comment_approved'     => 0,
        );



        $comment_id = wp_insert_comment( $data );

		update_field('field_5c3b16e68efcd', $tel, get_comment($comment_id) );
		update_field('field_5c3b1988859bd', $adv, get_comment($comment_id) );
		update_field('field_5c3b1b0ed73a7', $noadv, get_comment($comment_id) );
		update_field('field_5c3b1b2cd73a8', $prog, get_comment($comment_id) );
		update_field('field_5c3b1ba8d73a9', $food, get_comment($comment_id) );
		update_field('field_5c3b1bb9d73aa', $place, get_comment($comment_id) );
		update_field('field_5c3b1bc9d73ab', $infra, get_comment($comment_id) );
		update_field('field_5c3b1bd4d73ac', $price, get_comment($comment_id) );

	}
	return $WPCF7_ContactForm ;
}

function breadcrumbs() {

	/* === ОПЦИИ === */
	$text['home'] = 'Главная'; // текст ссылки "Главная"
	$text['category'] = '%s'; // текст для страницы рубрики
	$text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
	$text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
	$text['author'] = 'Статьи автора %s'; // текст для страницы автора
	$text['404'] = 'Ошибка 404'; // текст для страницы 404
	$text['page'] = 'Страница %s'; // текст 'Страница N'
	$text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

	$wrap_before = '<ul class="bread-crumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
	$wrap_after = '</ul><!-- .breadcrumbs -->'; // закрывающий тег обертки
	$sep = ''; // разделитель между "крошками"
	$before = '<li class="bread-crumbs__item"><span><span> '; // тег перед текущей "крошкой"
	$after = '</span></span></li>'; // тег после текущей "крошки"

	$show_on_home = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
	$show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
	$show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
	$show_last_sep = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
	/* === КОНЕЦ ОПЦИЙ === */

	global $post;
	$home_url = home_url('/');
	$link = '<li class="bread-crumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$link .= ' <a class="bread-crumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
	$link .= '<meta itemprop="position" content="%3$s" />';
	$link .= '</li>';
	$parent_id = ( $post ) ? $post->post_parent : '';
	$home_link = sprintf( $link, $home_url, $text['home'], 1 );

	if ( is_home() || is_front_page() ) {

		if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;

	} else {

		$position = 0;

		echo $wrap_before;

		if ( $show_home_link ) {
			$position += 1;
			echo $home_link;
		}

		if ( is_category() ) {
			$parents = get_ancestors( get_query_var('cat'), 'category' );
			foreach ( array_reverse( $parents ) as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			if ( false ) {
				$position += 1;
				$cat = get_query_var('cat');
				echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_search() ) {
			if ( $show_home_link && $show_current || ! $show_current && $show_last_sep ) echo $sep;
			if ( $show_current ) echo $before . sprintf( $text['search'], get_search_query() ) . $after;

		} elseif ( is_year() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_time('Y') . $after;
			elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_month() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_day() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
			$position += 1;
			echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_single() && ! is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$position += 1;
				$post_type = get_post_type_object( get_post_type() );
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
				elseif ( $show_last_sep ) echo $sep;
			} else {
				$cat = get_the_category(); $catID = $cat[0]->cat_ID;
				$parents = get_ancestors( $catID, 'category' );
				$parents = array_reverse( $parents );
				$parents[] = $catID;
				foreach ( $parents as $cat ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				}
				if ( get_query_var( 'cpage' ) ) {
					$position += 1;
					echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
					echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
				} else {
					if ( $show_current ) echo $sep . $before . get_the_title() . $after;
					elseif ( $show_last_sep ) echo $sep;
				}
			}

		} elseif ( is_post_type_archive() ) {
			$post_type = get_post_type_object( get_post_type() );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . $post_type->label . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_attachment() ) {
			$parent = get_post( $parent_id );
			$cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
			$parents = get_ancestors( $catID, 'category' );
			$parents = array_reverse( $parents );
			$parents[] = $catID;
			foreach ( $parents as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			$position += 1;
			echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_page() && ! $parent_id ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_title() . $after;
			elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_page() && $parent_id ) {
			$parents = get_post_ancestors( get_the_ID() );
			foreach ( array_reverse( $parents ) as $pageID ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
			}
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_tag() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$tagID = get_query_var( 'tag_id' );
				echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_author() ) {
			$author = get_userdata( get_query_var( 'author' ) );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_404() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . $text['404'] . $after;
			elseif ( $show_last_sep ) echo $sep;

		} elseif ( has_post_format() && ! is_singular() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			echo get_post_format_string( get_post_format() );
		}

		echo $wrap_after;

	}
}

function get_dropdown_term($term_name,$name,$placeholder,$class = null){
	$terms = get_terms( array(
		'taxonomy' => $term_name,
		'hide_empty' => false
	) );

	if(!is_for($terms)) return false;

	return create_dropdown_html($terms,$name,$placeholder,$class);
}

function get_dropdown_term_parent($term_name,$name,$placeholder,$class = null){
	$terms = get_terms( array(
		'taxonomy'   => $term_name,
		'parent'     => 0,
		'hide_empty' => false
	) );

	if(!is_for($terms)) return false;

	return create_dropdown_html($terms,$name,$placeholder,$class);
}

function create_dropdown_html($terms,$name,$placeholder,$class = null){
	$html = '';
	$html.= '<select data-placeholder="'.$placeholder.'" name="'.$name.'" class="search__select select-js '.$class.'">';
	$html.= '<option></option>';
	foreach ($terms as $term){
		$html.= '<option value="'.$term->slug.'">'.$term->name.'</option>';
	}
	$html.='</select>';

	return $html;
}

function get_dropdown_term_ajax($term_slug,$term_name){
	$term = get_term_by( 'slug', $term_slug, $term_name);
	$output = array(
		'result' => 'none'
	);


	if(empty($term)) die(json_encode($output));

	$terms = get_terms( array(
		'taxonomy'   => $term_name,
		'parent'     => $term->term_id,
		'hide_empty' => false
	) );

	if(!is_for($terms)) die(json_encode($output));

	$output = array();

	foreach ($terms as $v){
		$output[] = array(
			'name' => $v->name,
			'value' => $v->slug,
		);
	}

	die(json_encode($output));
}

add_action('wp_ajax_select', 'get_select_options');
add_action('wp_ajax_nopriv_select', 'get_select_options');

function get_select_options(){
	if($_POST['category'] === 'type' && !empty($_POST['value'])){

		$slug = $_POST['value'];

		get_dropdown_term_ajax($slug,'camp_type');
	}
	die;
}

add_action('pre_get_posts', 'search_by_cat');
function  search_by_cat() {

	if(!isset($_GET['post_type']) || $_GET['post_type'] !== 'product' ) return;

	global $wp_query;



		$category = $_GET['category'];
		$type     = $_GET['type'];
		$strana   = $_GET['strana'];
		$region   = $_GET['region'];
		$sezon    = $_GET['sezon'];

		if(!empty($category)){
			$wp_query->query_vars['tax_query'][] = array(
				"taxonomy" => "camp_type",
				"field"    => "slug",
				"terms"    =>  $category
			);
		}

		if(!empty($type)){
			$wp_query->query_vars['tax_query'][] = array(
				"taxonomy" => "camp_type",
				"field"    => "slug",
				"terms"    =>  $type
			);
		}

		if(!empty($strana)){
			$wp_query->query_vars['tax_query'][] = array(
				"taxonomy" => "pa_strana",
				"field"    => "slug",
				"terms"    =>  $strana
			);
		}

		if(!empty($region)){
			$wp_query->query_vars['tax_query'][] = array(
				"taxonomy" => "pa_region",
				"field"    => "slug",
				"terms"    =>  $region
			);
		}

		if(!empty($sezon)){
			$wp_query->query_vars['tax_query'][] = array(
				"taxonomy" => "pa_sezon",
				"field"    => "slug",
				"terms"    =>  $sezon
			);
		}


}


function get_related_posts(){
	global $post;

	$tags = wp_get_post_tags($post->ID);

	if ($tags) {
		$tag_ids = array();
		foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
	}
	$args = array(
		'post__not_in'        => array($post->ID),
		'posts_per_page'      => 5,
		'ignore_sticky_posts' => 1,
		'post_status'         => 'publish',
		'orderby'             => 'rand',

	);

	if(!empty($tag_ids)) $args['tag__in'] = $tag_ids;

	$output =  new wp_query($args);

	wp_reset_query();
	wp_reset_postdata();

	return $output;
}

function new_excerpt_more( $more ) {
	return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function get_news(){
	return get_posts(array(
		'category'    => 1
	));
}

//WOOCOMMERCE
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );


remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
//remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10 );

//remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );

//remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );



//Добавляем хуки
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
add_action( 'camp_product_meta', 'woocommerce_template_single_meta', 40 );
add_action( 'camp_product_tabs', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'camp_product_attributes', 'wc_display_product_attributes', 10 );
add_action( 'camp_product_single_title', 'woocommerce_template_single_title', 5 );
add_action( 'camp_product_related', 'woocommerce_output_related_products', 20 );
add_action( 'camp_product_breadcrumb', 'woocommerce_breadcrumb', 20, 0 );
add_action( 'camp_product_loop_thumbnail', 'woocommerce_template_loop_product_thumbnail', 10 );

//* Enqueue scripts and styles
//add_action( 'wp_enqueue_scripts', 'crunchify_disable_woocommerce_loading_css_js' );
function crunchify_disable_woocommerce_loading_css_js() {

	// Check if WooCommerce plugin is active
	if( function_exists( 'is_woocommerce' ) ){

		// Check if it's any of WooCommerce page
		if(! is_woocommerce() && ! is_cart() && ! is_checkout() ) {

			## Dequeue WooCommerce styles
			wp_dequeue_style('woocommerce-layout');
			wp_dequeue_style('woocommerce-general');
			wp_dequeue_style('woocommerce-smallscreen');

			## Dequeue WooCommerce scripts
			wp_dequeue_script('wc-cart-fragments');
			wp_dequeue_script('woocommerce');
			wp_dequeue_script('wc-add-to-cart');

		}
	}
}

add_filter('woocommerce_attribute','remove_attr_tags',10);
function remove_attr_tags($attr){
	return strip_tags($attr);
}

add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
		case 'RUB': $currency_symbol = ' руб.'; break;
	}
	return $currency_symbol;
}

function get_avg_rating($id){
    global $wpdb;

    $sql = "SELECT AVG(meta_value)  FROM {$wpdb->prefix}commentmeta WHERE comment_id IN (SELECT comment_ID FROM {$wpdb->prefix}comments WHERE comment_post_ID = ".(int)$id." AND comment_approved = 1) AND meta_key IN ('prog','food','place','infra','price')";

    $result = $wpdb->get_row ($sql,ARRAY_N);
    if(isset($result[0])){
        return $result[0];
    }
    return false;
}

function get_avg_rating_string($num){
	$num = round($num);
    switch ($num) {
        case 1:
            return "очень плохо";
            break;
        case 2:
	        return "плохо";
            break;
        case 3:
	        return "нормально";
            break;
        case 4:
	        return "хорошо";
            break;
        case 5:
	        return "отлично";
            break;
    }
    return false;
}

add_filter('woocommerce_dropdown_variation_attribute_options_html','change_variation_select');

function change_variation_select($html){
	return $html;
}

add_filter('woocommerce_show_variation_price',      function() { return TRUE;});


function get_old_price($product){
	if($product->is_on_sale() && $product->product_type == 'variable'){
		$price = $product->get_variation_regular_price( 'min', true );
	}elseif($product->is_on_sale() && $product->product_type == 'simple'){
		$price = $product->get_regular_price();
	}else{
		return false;
	}

	return wc_price($price);
}

function get_price($product){
	if($product->is_on_sale() && $product->product_type == 'variable'){
		$price = $product->get_variation_sale_price( 'min', true );
	}elseif($product->product_type == 'variable'){
		$price = $product->get_variation_regular_price( 'min', true );
	}else{
		$price = $product->get_price();
	}

	if (empty($price)){
		return '<span class="woocommerce-Price-amount amount">уточняйте</span>';
	}

	return wc_price($price);
}
