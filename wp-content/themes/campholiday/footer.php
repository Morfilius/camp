<footer class="footer">
    <div class="container">
        <div class="footer-items">
            <div class="footer-item">
                <?php if($address = get_field('address','option')):?>
                    <p class="footer-address">
                       <?php echo $address?>
                    </p>
                <?php endif?>

                <a href='<?php echo get_tel(get_field('phone','option'))?>' class="footer-tel">Тел.: <strong><?php the_field('phone','option')?></strong></a>
                <div class="tal">
                    <a data-fancybox data-src="#callback-form" href="javascript:;" class="button callback-footer"><span>Обратная связь</span></a>
                </div>
            </div>
            <div class="footer-item">
	            <?php $args = array(
		            'theme_location' => 'footer-1',
		            'container'=> false,
		            'menu_class' => 'footer-menu',
	            );
	            wp_nav_menu($args);
	            ?>
            </div>
            <div class="footer-item">
	            <?php $args = array(
		            'theme_location' => 'footer-2',
		            'container'=> false,
		            'menu_class' => 'footer-menu',
	            );
	            wp_nav_menu($args);
	            ?>
            </div>
            <div class="footer-item">
	            <?php $args = array(
		            'theme_location' => 'footer-3',
		            'container'=> false,
		            'menu_class' => 'footer-menu',
	            );
	            wp_nav_menu($args);
	            ?>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-items">
                <div class="footer-bottom-item">
                    <div class="copy">
                        © CampHoliday.ru <span>2007 - <?php echo date('Y')?></span><br>
                        <?php if(is_active_sidebar( 'footer-1' )):?>
	                        <?php dynamic_sidebar('footer-1');?>
                        <?php endif?>
                    </div>
                </div>
                <div class="footer-bottom-item">
                    <div class="company">
	                    <?php if(is_active_sidebar( 'footer-2' )):?>
		                    <?php dynamic_sidebar('footer-2');?>
	                    <?php endif?>
                    </div>
                </div>
                <div class="footer-bottom-item">
                    <div class="footer-links">
	                    <?php if(is_active_sidebar( 'footer-3' )):?>
		                    <?php dynamic_sidebar('footer-3');?>
	                    <?php endif?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="up-button"></div>
    <div style="display: none">
        <div id="callback-form" class="callback-form">
            <span class="callback-form__title">Обратный звонок</span>
            <?php echo do_shortcode('[contact-form-7 id="20356" title="Форма обратной связи в шапке"]')?>
        </div>
	    <?php get_template_part('parts/common/rev-form')?>
    </div>
</footer>

<?php wp_footer()?>
</div>
</body>
</html>