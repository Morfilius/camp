<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}?>


<?php if(! empty( $breadcrumb )):?>
	<ul class="bread-crumbs" itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<?php $i = 0;$count = count($breadcrumb)?>
		<?php foreach($breadcrumb as $key => $crumb):?>
            <?php if(empty($crumb[1])) continue; $i++ ?>
			<?php if($count != $key + 1):?>
				<li class="bread-crumbs__item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<a class="bread-crumbs__link" href="<?php echo $crumb[1]?>" itemprop="item">
						<span itemprop="name"><?php echo $crumb[0]?></span>
					</a>
					<meta itemprop="position" content="<?php echo $i + 1?>">
				</li>
			<?php else:?>
            <li class="bread-crumbs__item">
                <span>
                    <span><?php echo $crumb[0]?></span>
                </span>
            </li>
			<?php endif?>

		<?php endforeach?>


	</ul>
<?php endif?>

