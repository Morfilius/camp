<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<main class="content">
	<div class="container">
        <?php do_action('camp_product_breadcrumb')?>
		<!--ul class="bread-crumbs">
			<li class="bread-crumbs__item">
				<a class="bread-crumbs__link" href="/">
					<span>Главная</span>
				</a>
			</li>
			<li class="bread-crumbs__item">
				<a class="bread-crumbs__link" href="/">
					<span>Детские лагеря</span>
				</a>
			</li>
			<li class="bread-crumbs__item">
                <span>
                    <span>Детский лагерь Джуниор Кэмп в Болгарии</span>
                </span>
			</li>
		</ul-->
	</div>

