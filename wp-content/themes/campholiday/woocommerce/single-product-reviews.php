<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! comments_open() ) {
	return;
}

$comments = get_comments(array(
        'post_id' => $product->get_id()
));

$sql = "SELECT AVG(meta_value)  FROM wp_commentmeta WHERE comment_id IN (8,9) AND meta_key IN ('prog','food','place','infra','price')";

?>
<?php if(is_for($comments)):?>
    <div class="reviews">
        <?php foreach($comments as $comment):?>
            <div class="reviews-item">
                <span class="reviews-item__title"><?php echo $comment->comment_author?>, <span class="reviews-item__date"><?php echo get_comment_date( 'd.m.Y', $comment->comment_ID );?></span></span>
                <?php if(get_field('adv',$comment)):?>
                    <div class="reviews-item__block">
                        <span class="reviews-item__block-title">Преимущества:</span>
                        <p class="reviews-item__block-text">
			                <?php the_field('adv',$comment)?>
                        </p>
                    </div>
                <?php endif?>
                <?php if(get_field('noadv',$comment)):?>
                    <div class="reviews-item__block">
                        <span class="reviews-item__block-title">Недостатки:</span>
                        <p class="reviews-item__block-text">
			                <?php the_field('noadv',$comment)?>
                        </p>
                    </div>
                <?php endif?>

                <div class="ratings-container">
                    <table class="ratings">
                        <tr class="ratings__row">
                            <td class="ratings__title ratings__td">Программа:</td>
                            <td class="ratings__stars ratings__td"><span data-score="<?php the_field('prog',$comment)?>" class="stars"></span></td>
                        </tr>
                        <tr class="ratings__row">
                            <td class="ratings__title ratings__td">Питание:</td>
                            <td class="ratings__stars ratings__td"><span data-score="<?php the_field('food',$comment)?>" class="stars"></span></td>
                        </tr>
                        <tr class="ratings__row">
                            <td class="ratings__title ratings__td">Условия размещения:</td>
                            <td class="ratings__stars ratings__td"><span data-score="<?php the_field('place',$comment)?>" class="stars"></span></td>
                        </tr>
                        <tr class="ratings__row">
                            <td class="ratings__title ratings__td">Инфраструктура:</td>
                            <td class="ratings__stars ratings__td"><span data-score="<?php the_field('infra',$comment)?>" class="stars"></span></td>
                        </tr>
                        <tr class="ratings__row">
                            <td class="ratings__title ratings__td">Цена-качество:</td>
                            <td class="ratings__stars ratings__td"><span data-score="<?php the_field('price',$comment)?>" class="stars"></span></td>
                        </tr>
                    </table>
                </div>
            </div>
        <?php endforeach?>

    </div>
<?php else:?>
    <p>
        Отзывов пока нет.<br>

        Будьте первым, кто оставил отзыв на <?php the_title()?>
        Ваш телефон не будет опубликован.
    </p>
<?php endif?>




<div style="display: none">
    <div id="review-form"  class="callback-form">
        <span class="callback-form__title">Обратный звонок</span>
		<?php echo do_shortcode('[contact-form-7 id="1480" title="Форма отзыва"]');?>
    </div>
</div>


