<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php if($related_products):?>
    <div class="camp-related">
        <div class="camp-related__title">Похожие предложения</div>
        <div id="camp-related" class="article-items camp-related-items">
			<?php foreach($related_products as $related_product):?>
                <?php
                    global $post;
                    $post = get_post($related_product->get_id());
                    setup_postdata($post);
                    global $product;
                ?>

                <div class="article-item camp-item">
                    <div class="article-item__inner camp-item__inner">
                        <div class="article-item__img-wrap camp-item__img-wrap">
                            <a href="<?php the_permalink()?>" class="article-item__img-inner grd-blue">
                                <img data-lazy="<?php echo get_the_post_thumbnail_url($post->ID,'prod-thumb')?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="alt" class="article-item__img">
                            </a>
                        </div>
                        <footer class="article-item__bottom camp-item__bottom">
                            <a href='<?php the_permalink()?>' class="article-item__title camp-item__title">
                                <?php the_title()?>
                            </a><br>
	                        <?php if ( $price_html = $product->get_price_html() ) : ?>
                                <span class="article-item__color camp-item__color"><?php echo $price_html ?></span>
	                        <?php endif; ?>
                        </footer>
                        <div class="tac camp-item__btn-wrap">
                            <a href="<?php the_permalink()?>" class="button camp-item__link"><span>Подробнее</span></a>
                        </div>
                    </div>
                </div>
			<?php endforeach?>
        </div>
    </div>
    <?php wp_reset_postdata()?>
<?php endif?>



