<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="tags <?php echo is_single() ? 'camp-content__tags' : 'camp-info__tags';?>">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

    <?php $tags = get_terms_post($product->get_id(),'product_tag')?>

    <?php if(is_for($tags)):?>
        <?php foreach($tags as $tag):?>
            <a href="<?php echo get_term_link($tag->term_id)?>" class="tags__tag camp-content__tag">
                <?php echo $tag->name?>
            </a>
        <?php endforeach?>
    <?php endif?>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>
