<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $product;

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
$rating = get_avg_rating($product->get_id());
?>
<div class="tabs">
    <ul class="tabs-menu">
        <li class="tabs-menu__item">
            <a class="tabs-menu__link" href="#tab-1">Описание</a>
        </li>
        <?php if(get_field('prog_text')):?>
            <li class="tabs-menu__item">
                <a class="tabs-menu__link" href="#tab-2">Программа</a>
            </li>
        <?php endif?>
        <?php if(get_field('price_text')):?>
            <li class="tabs-menu__item">
                <a class="tabs-menu__link" href="#tab-3">Cтоимость</a>
            </li>
        <?php endif?>
        <?php if(get_field('transfer_text')):?>
            <li class="tabs-menu__item">
                <a class="tabs-menu__link" href="#tab-4">Трансфер</a>
            </li>
        <?php endif?>
        <?php if(get_field('prepare_text')):?>
            <li class="tabs-menu__item">
                <a class="tabs-menu__link" href="#tab-5">Подготовка к поездке</a>
            </li>
        <?php endif?>
        <li class="tabs-menu__item">
            <a class="tabs-menu__link" href="#tab-6">Отзывы</a>
        </li>
    </ul>
    <div class="camp-desc" id="tab-1">
        <div class="camp-desc-info__wrap">
            <table class="camp-desc-info">
                <?php get_template_part('parts/woo/region','single')?>
                <?php do_action('camp_product_attributes',$product)?>
                <tr class="camp-desc-info__row">
                    <td class="camp-desc-info__title">
                        <span>Рейтинг</span>
                    </td>
                    <td class="camp-desc-info__text">
                        <div data-score="<?php echo round($rating)?>" class="stars"></div><span class="bold"><?php echo $rating ? round($rating,1).' ('.get_avg_rating_string($rating).')' : ''?></span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="camp-desc-text">
	        <?php get_template_part('parts/constructor/index')?>
        </div>
    </div>
    <?php if(get_field('prog_text')):?>
        <div class="camp-prog" id="tab-2">
            <div class="camp-prog__text">
                <?php the_field('prog_text')?>
            </div>
        </div>
    <?php endif?>
    <?php if(get_field('price_text')):?>
        <div class="camp-price" id="tab-3">
                <?php if($text = get_field('text')):?>
                    <div class="camp-price__text">
                        <?php echo $text?>
                    </div>
                <?php endif?>
	            <?php if( have_rows('table') ):?>
                <div class="camp-price__item">
                    <table class="table">
                        <tr class="table__row">
                            <th class="table__th">Номер смены</th>
                            <th class="table__th">Даты смены</th>
                            <th class="table__th">Стоимость путевки </th>
                            <th class="table__th">Стоимость трансфера</th>
                        </tr>
			            <?php while ( have_rows('table') ) : the_row();?>
                            <tr class="table__row">
                                <th class="table__td"><?php the_sub_field('number')?></th>
                                <th class="table__td"><?php the_sub_field('dates')?></th>
                                <th class="table__td"><strong class="price-orange"><?php the_sub_field('prices')?></strong> руб.</th>
                                <th class="table__td"><?php the_sub_field('transer')?> руб.</th>
                            </tr>
			            <?php endwhile;?>
                    </table>
                </div>
	            <?php endif;?>
            <div class="camp-price__text">
                <?php the_field('price_text')?>
            </div>
        </div>
    <?php endif?>
    <?php if(get_field('transfer_text')):?>
        <div class="camp-transf" id="tab-4">
            <div class="camp-transf__text">
                <?php the_field('transfer_text')?>
            </div>
        </div>
    <?php endif?>
    <?php if(get_field('prepare_text')):?>
        <div class="camp-prep" id="tab-5">
            <div class="camp-prep__text">
                <?php the_field('prepare_text')?>
            </div>
        </div>
    <?php endif?>

    <div class="camp-review" id="tab-6">
	    <?php get_template_part('parts/woo/reviews')?>
        <div class="tac">
            <a data-fancybox data-src='#review-form' href="javascript:;" class="button reviews-button"><span>Оставить отзыв</span></a>
        </div>
    </div>

</div>
