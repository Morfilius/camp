<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart booking-form search" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

            <?php echo $product->get_price_html()?>
			<?php if(!empty( $available_variations ) && false !== $available_variations ):?>
				<?php foreach($attributes as $attribute_name => $options):?>
                    <label class="search__select-wrap booking-form__select-wrap">
						<?php
						wc_dropdown_variation_attribute_options( array(
							'options'   => $options,
							'attribute' => $attribute_name,
							'product'   => $product,
							'class' => 'booking-form__select search__select'
						) );

						?>
                    </label>
				<?php endforeach?>

			<?php endif?>
            <div class="booking-form__bottom">
                <div class="counter">
                    <label>
                        <span class="counter__minus">-</span>
                        <input type="number" class="counter__input" value="1" name="cnt">
                        <span class="counter__plus">+</span>
                    </label>
                </div>
                <button class="button booking-form__button"><span>Забронировать</span></button>
            </div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
