<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>

<?php

	?>

    <section class="camps">
        <div class="container">
	        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                <h1 class="title"><?php woocommerce_page_title(); ?></h1>
	        <?php endif; ?>
            <?php if(woocommerce_product_loop() ):?>
            <div class="camps-top">
                <?php do_action( 'woocommerce_before_shop_loop' );?>
                <a href="#map" class="camps-top__link">Лагеря на карте</a>
            </div>
            <div class="camps-items">

	                <?php woocommerce_product_loop_start();?>
	                <?php
	                if ( wc_get_loop_prop( 'total' ) ) {
		                while ( have_posts() ) {
			                the_post();

			                /**
			                 * Hook: woocommerce_shop_loop.
			                 *
			                 * @hooked WC_Structured_Data::generate_product_data() - 10
			                 */
			                do_action( 'woocommerce_shop_loop' );

			                wc_get_template_part( 'content', 'product' );
		                }
	                }
	                ?>

            </div>
            <?php do_action( 'woocommerce_after_shop_loop' );?>
            <div id="map" class="camp-map-container">
                <h2 class="camp-map__title">Лагеря на карте</h2>
                <p>В разработке</p>
<!--                <div class="camp-map">-->
<!--                    <img src="img/camp-map.png" alt="alt" style="width: 100%">-->
<!--                </div>-->
            </div>
	            <?php woocommerce_product_loop_end()?>
            <?php else:?>
                <p>По вашему запросу ничего не найдено.</p>
            <?php endif?>
        </div>
    </section>
    <?

do_action( 'woocommerce_after_main_content' );

get_footer( );
