<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$rating = get_avg_rating($product->get_id());
?>

<div class="camps-item">
    <div class="camps-item__top">
        <span class="camps-item__title camps-item__h2"><a href="<?php the_permalink()?>"><?php the_title()?></a></span>
        <?php if($rating):?>
            <div data-score="<?php echo round($rating)?>" class="stars camps-item__stars"></div>
        <?php endif?>
        <span class="camps-item__rating"><?php echo $rating ? round($rating,1).' ('.get_avg_rating_string($rating).')' : ''?></span>
    </div>
    <div class="camps-item__content">
        <div class="camps-item__img-container">
            <span class="camps-item__subtitle camps-item__h3"><?php get_template_part('parts/woo/region','loop')?></span>
            <div class="camps-item__img-wrap">
                <a href="<?php the_permalink()?>" class="camps-item__inner">
                    <img data-src="<?php echo get_the_post_thumbnail_url($product->get_id(),'prod-thumb')?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_title()?>" class="camps-item__img lazyload">
                </a>
            </div>
        </div>
        <div class="camps-info">
	        <?php do_action('camp_product_meta')?>

            <div class="camps-info__inner">
                <div class="camps-info__text">
                    <?php the_excerpt()?>
                </div>
                <div class="camps-info__bottom">
                    <div class="camps-info__shifts">
                        <div class="camps-info__shift">
                            <div class="camps-info__date">28 октября - 4 ноября</div>
                            <div class="camps-info__prices">
                                <span class="camps-info__old-price"><?php echo get_old_price($product)?></span>
                                <div class="camps-info__price-wrap">
                                    <span class="camps-info__new-price"><?php echo get_price($product)?></span><br>
                                </div>
                            </div>
                        </div>
                        <div class="camps-info__shift">
                            <a href="<?php the_permalink()?>" class="camps-info__shift-link">Посмотреть другие смены</a>
                            <span class="camps-info__offer"><?php get_template_part('parts/woo/benefit')?></span>
                        </div>
                    </div>
                    <div class="camps-info__button">
                        <a href="<?php the_permalink()?>" class="button camps-info__link">
                            <span>Подробнее</span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!--li <?php wc_product_class(); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li-->
