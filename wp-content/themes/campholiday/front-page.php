<?php get_header()?>
	<!--content-->
	<main class="content">
		<div class="home-slider-sect">
			<div class="container">
				<div class="search-sliders">
					<div class="search-sliders__left">
						<?php get_template_part('parts/woo/search-form')?>
					</div>
					<div class="search-sliders__right">
						<?php get_template_part('parts/common/home-slider')?>
						<div class="mobile-gallery">
							<div class="mobile-gallery__inner-container">
								<div class="mobile-gallery__item">
									<div class="mobile-gallery__inner">
										<div class="mobile-gallery__wrap">
											<span class="mobile-gallery__title">Московская область1</span>
											<img  class="mobile-gallery__img" src="<?php bloginfo('template_url'); ?>/img/oboi.jpg" alt="alt">
										</div>
									</div>
								</div>
								<div class="mobile-gallery__item">
									<div class="mobile-gallery__inner">
										<div class="mobile-gallery__wrap">
											<span class="mobile-gallery__title">Московская область2</span>
											<img  class="mobile-gallery__img" src="<?php bloginfo('template_url'); ?>/img/oboi.jpg" alt="alt">
										</div>
									</div>
								</div>
								<div class="mobile-gallery__item">
									<div class="mobile-gallery__inner">
										<div class="mobile-gallery__wrap">
											<span class="mobile-gallery__title">Московская область3</span>
											<img  class="mobile-gallery__img" src="<?php bloginfo('template_url'); ?>/img/oboi.jpg" alt="alt">
										</div>
									</div>
								</div>
								<div class="mobile-gallery__item">
									<div class="mobile-gallery__inner">
										<div class="mobile-gallery__wrap">
											<span class="mobile-gallery__title">Московская область4</span>
											<img  class="mobile-gallery__img" src="<?php bloginfo('template_url'); ?>/img/oboi.jpg" alt="alt">
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
        <?php get_template_part('parts/page-constructor/index')?>

	</main>
	<!--content end -->
<?php get_footer()?>