<?php
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header();?>
	<main class="content">
		<div class="container">
			<?php breadcrumbs()?>
		</div>
		<section class="contacts">
			<div class="container">
				<?php if (have_posts()) : ?>
					<?while (have_posts()) : the_post();?>
						<?php
							$product = wc_get_product(1461);
							$options = $product->get_variation_attributes();
						    $available_variations = $product->get_available_variations()
						?>
						<section class="booking-sect">
							<div class="container">
								<h1 class="title">
									<?php the_title()?>
								</h1>

								<div class="booking-form-container variations_form" data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo htmlspecialchars( wp_json_encode( $available_variations ) ); // WPCS: XSS ok. ?>">
									<div class="booking-form callback-form variations">
                                       <?php echo do_shortcode('[contact-form-7 id="20402" title="Бронирование"]')?>

									</div>
								</div>
							</div>
						</section>
						<?php get_template_part('parts/page-constructor/index')?>
					<?php endwhile;?>
				<?else: echo '<p>Нет записей.</p>';?>
				<? endif;?>
			</div>
		</section>
	</main>
<?php get_footer(); // подключаем footer.php ?>