<?php
get_header();?>
<main class="content">
    <div class="container">
    <?php breadcrumbs()?>
    </div>
	<?php if (have_posts()) : ?>

		<?while (have_posts()) : the_post();?>
    <section class="single-blog">
        <div class="container">

            <h1 class="title"><?php the_title()?></h1>
            <div class="single-blog-text">
                <?php $gallery = get_field('gallery')?>
                <?php if($gallery && count($gallery) > 1):?>
                    <div class="post-gallery">
                        <?php foreach($gallery as $image):?>
                            <div>
                                <img src="<?php echo $image['url']?>" alt="<?php the_title()?>">
                            </div>
                        <?php endforeach?>
                    </div>
                <?php else:?>
                    <div class="post-thumb">
		                <?php the_post_thumbnail('full')?>
                    </div>
                <?php endif?>

                <p></p>
                <?php the_content()?>
            </div>
        </div>
    </section>

	<?php endwhile;?>
        <?php get?>
	<? endif;?>
</main>
<?php get_footer();?>
