<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=cyrillic" rel="stylesheet">
    <?php wp_head()?>
    <script>
        var wp_path = '<?php bloginfo('template_url'); ?>';
        var myajax = '<?php echo admin_url('admin-ajax.php');?>';
    </script>
</head>
<body>
<div class="content-wrapper">
    <!--header start-->
    <header class="header">
        <div class="header-wrap">
            <div class="container">
                <div class="header-top">
                    <a class="button-burger" id="burger">
                        <span class="button-burger__icon"></span>
                    </a>
                    <?php if(!is_front_page()):?>
                    <a href="<?php echo site_url()?>" class="header-logo-container">
                        <?php if($logo = get_field('logo','option')):?>
                            <img src="<?php echo $logo['url']?>" alt="logo" class="header-logo">
                        <?php endif?>
                        <?php if($logo_mob = get_field('logo-mob','option')):?>
                            <img src="<?php echo $logo_mob['url']?>" alt="logo" class="header-logo-mob">
                        <?php endif?>
                    </a>
	                <?php else:?>
                        <span class="header-logo-container">
		                    <?php if($logo = get_field('logo','option')):?>
                                <img src="<?php echo $logo['url']?>" alt="logo" class="header-logo">
		                    <?php endif?>
		                    <?php if($logo_mob = get_field('logo-mob','option')):?>
                                <img src="<?php echo $logo_mob['url']?>" alt="logo" class="header-logo-mob">
		                    <?php endif?>
                        </span>
                    <?php endif?>
                    <div class="top-menu-container">
                        <?php $args = array(
                                    'theme_location' => 'top',
                                    'container'=> false,
                                    'menu_class' => 'top-menu',
                                );
                                wp_nav_menu($args);
                        ?>
                    </div>
                    <div class="actions-container">
                        <div class="header-phone-container">
                            <a href="<?php echo get_tel(get_field('phone','option'))?>" class="header-phone phone"><?php the_field('phone','option')?></a>
                        </div>
                        <div class="header-button-container">
                            <a data-fancybox data-src="#callback-form" href="javascript:;" class="button callback-button"><span>Обратная связь</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-mob-bg">
                <div class="container">
                    <div class="header-bottom">
                        <?php $args = array(
                                    'theme_location' => 'main',
                                    'container'=> false,
                                    'menu_class' => 'main-menu',
                                );
                                wp_nav_menu($args);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--header end-->