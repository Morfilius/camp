<?php if( have_rows('page-constructor') ):?>

	<?php while ( have_rows('page-constructor') ) : the_row(); ?>

		<?php if( get_row_layout() == 'links_1_block' ):?>

			<?php get_template_part('parts/page-constructor/links-1-block')?>

		<?php elseif( get_row_layout() == 'special_offers_block' ):?>

			<?php get_template_part('parts/page-constructor/special-offers-block')?>

		<?php elseif( get_row_layout() == 'links_2_block' ):?>

			<?php get_template_part('parts/page-constructor/links-2-block')?>

		<?php elseif( get_row_layout() == 'ratings_block' ):?>

			<?php get_template_part('parts/page-constructor/ratings-block')?>

		<?php elseif( get_row_layout() == 'news_block' ):?>

			<?php get_template_part('parts/page-constructor/news-block')?>

		<?php elseif( get_row_layout() == 'text_block' ):?>

			<?php get_template_part('parts/page-constructor/text-block')?>

		<?php elseif( get_row_layout() == 'address_block' ):?>

			<?php get_template_part('parts/page-constructor/address-block')?>

		<?php elseif( get_row_layout() == 'form_block' ):?>

			<?php get_template_part('parts/page-constructor/form-block')?>

		<?php endif;?>

	<?php endwhile;?>

<?php else :?>

<?php endif;