<div class="row">
	<div class="col-lg-6">
		<div class="office">
            <?php if($title = get_sub_field('title')):?>
                <h2 class="office__title"><?php echo $title?></h2>
            <?php endif?>

			<div class="office__text">
				<?php the_sub_field('address')?>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="contact-map">
			<div class="contact-map__inner">
				<?php the_sub_field('map')?>
			</div>
		</div>
	</div>
</div>