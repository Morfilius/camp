<?php if( have_rows('links') ):?>
    <div class="container container-mob">
        <div class="offer-text-items">
    <?php while ( have_rows('links') ) : the_row();?>
        <div class="offer-text-item">
            <p class="offer-text-item__content">
                <?php the_sub_field('text')?>
            </p>
            <a href="<?php the_sub_field('link')?>" class="button offer-text-item__button">
                <span><?php the_sub_field('link_text')?></span>
            </a>
        </div>
    <?php endwhile;?>
        </div>
    </div>
<?php endif;?>
