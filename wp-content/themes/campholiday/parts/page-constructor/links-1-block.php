<div class="registry-sect">
	<div class="container">
		<div class="registry-info">
			<div class="registry-info__item-text">
				<?php the_sub_field('desc')?>
			</div>
            <?php if( have_rows('links') ):?>
                <?php while ( have_rows('links') ) : the_row();?>
                    <div class="button registry-info__item">
                        <a href="<?php the_sub_field('link')?>" class="registry-info__item-inner">
                            <?php if($image = get_sub_field('image')):?>
                                <img src="<?php echo $image['url']?>" alt="<?php echo $image['alt'] ? $image['alt'] : 'alt';?>" class="registry-info__img">
                            <?php endif?>
                        </a>
                    </div>
                <?php endwhile;?>
            <?php endif;?>
		</div>
	</div>
</div>