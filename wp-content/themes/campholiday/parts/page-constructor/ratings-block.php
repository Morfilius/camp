
<div class="section rating-sect">
    <div class="container container-mob">
        <span class="section__title"><?php the_sub_field('title')?></span>
        <div class="rating-sect__inner section__inner">
            <?php if( have_rows('ratings') ):?>
            <div class="rating-items">
            <?php while ( have_rows('ratings') ) : the_row();?>
                <?php $post = get_sub_field('special_offer'); setup_postdata($post)?>
                <div class="rating-item">
                    <div class="rating-item__inner">
                        <div class="rating-item__img-wrap">
                            <a href="<?php the_permalink()?>" class="rating-item__img-inner grd-blue">
                                <img data-src="<?php echo get_the_post_thumbnail_url($post->ID,'prod-thumb-mob')?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_title()?>" class="lazyload rating-item__img">
                            </a>
                        </div>
                        <footer class="rating-item__bottom">
                            <a href='<?php the_permalink()?>' class="rating-item__title">
                                <?php the_title()?>
                            </a>
                            <a href="<?php the_permalink()?>" class="rating-item__link">Читать дальше</a>
                        </footer>
                    </div>
                </div>
                <?php wp_reset_postdata()?>
            <?php endwhile;?>
            </div>
            <?php endif;?>
        </div>
        <div class="slider-arrows" id="rating-arrows"></div>
    </div>
</div>





