<div class="section offers-sect">
	<div class="container container-mob">
		<span class="section__title"><?php the_sub_field('title')?></span>
        <?php if( have_rows('special_offers') ):?>
            <div class="offers-sect__inner">
            <?php while ( have_rows('special_offers') ) : the_row();?>
                <?php $post = get_sub_field('special_offer'); setup_postdata($post); global $product;?>
                <div  class="offers-item">
                    <div class="offers-item__inner">
                        <div class="offers-item__img-wrap">
                            <a href="<?php the_permalink()?>" class="offers-item__img-inner grd-blue">
                                <img data-src="<?php echo get_the_post_thumbnail_url($post->ID,'blog-loop')?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_title()?>" class="lazyload offers-item__img">
                            </a>
                        </div>
                        <div class="offers-item__text-wrap">
                            <a href="<?php the_permalink()?>" class="offers-item__title"><?php the_title()?></a><br>
                            <span class="offers-item__title-color">от <?php echo get_price($product)?></span>
                        </div>
                    </div>
                </div>
                <?php wp_reset_postdata()?>
            <?php endwhile;?>
            </div>
            <div class="slider-arrows" id="offer-arrows"></div>
        <?php endif;?>
	</div>
</div>