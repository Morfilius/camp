<div class="section news-sect">
	<div class="container container-news">
		<span class="section__title"><?php the_sub_field('title')?></span>
		<div class="section__inner news-sect__inner">
			<div id='articles' class="article-items">
                <?php
                    $items = get_sub_field('news');
                    if(!is_for($items)) {
                        $items = get_news();
                    }else{
	                    $items = array_column($items,'special_offer');
                    };
                ?>
                <?php if(is_for($items)):?>
                    <?php foreach($items as $post):?>
                        <?php setup_postdata($post)?>
                        <div class="article-item">
                            <div class="article-item__inner">
                                <div class="article-item__img-wrap">
                                    <a href="<?php the_permalink()?>" class="article-item__img-inner grd-blue">
                                        <img data-lazy="<?php echo get_the_post_thumbnail_url($post->ID,'prod-info')?>" src="<?php bloginfo('template_url'); ?>/img/placeholder.png" alt="<?php the_title()?>" class="article-item__img">
                                    </a>
                                </div>
                                <footer class="article-item__bottom">
                                    <a href='<?php the_permalink()?>' class="article-item__title">
                                       <?php the_title()?>
                                    </a>
                                    <span class="article-item__color"><?php echo get_the_date('d.m.Y')?></span>
                                </footer>
                            </div>
                        </div>
                    <?php endforeach?>
                  <?php wp_reset_postdata()?>
                <?php endif?>
			</div>
		</div>
		<div class="slider-arrows news-arrows" id="news-arrows"></div>
	</div>
</div>