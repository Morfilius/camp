<div class="col-md-6 col-sm-12">
	<article class="article">
		<?php if($image = get_the_post_thumbnail_url(null,'blog-loop')):?>
			<div class="article__img-wrap">
				<a href="<?php the_permalink()?>" class="article__img-inner">
					<img data-src="<?php echo $image?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_title()?>" class="lazyload article__img">
				</a>
			</div>
		<?php endif?>
		<span class="article__title h2">
            <a href="<?php the_permalink()?>"><?php the_title()?></a>
        </span>
		<p class="article__desc">
			<?php the_excerpt()?>
		</p>
	</article>
</div>