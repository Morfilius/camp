<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;
global $product;

$attachment_ids = $product->get_gallery_image_ids();?>

<?php if(is_for($attachment_ids)):$i=0?>
<?php $count = count($attachment_ids)?>
<div class="camp-thumb">
	<?php foreach($attachment_ids as $gallery_id):$i++?>

		<?php echo $i === 1 ? '<div class="camp-thumb__items">' : ''?>

		<?php if($i <= 4):?>
			<?php $image = wp_get_attachment_image_src($gallery_id,'prod-thumb')[0]?>
			<div class="camp-thumb__item <?php echo ($i === 4 && $count > 4) ? 'more' : '';?>">
				<div class="camp-thumb__wrap">
					<a <?php echo ($i === 4 && $count > 4) ? 'id="img-more"' : '';?> href="<?php echo wp_get_attachment_image_src($gallery_id,'full')[0]?>" data-fancybox="images" class="camp-thumb__inner">
						<img src="<?php echo $image?>" alt="<?php echo get_the_title()?>" class="camp-thumb__img">
					</a>
				</div>
			</div>
		<?php endif?>

		<?php echo ($i < 4 && $count === $i) ? '</div>' : '';?>
		<?php echo ($i === 4) ? '</div>' : '';?>



		<?php echo ($i === 5) ? '<div id="more-gallery" class="camp-thumb-items-more">' : '';?>

		<?php if($i > 4):?>
			<?php $image = wp_get_attachment_image_src($gallery_id,'prod-thumb')[0]?>
			<div class="camp-thumb__item camp-thumb-items-more__item">
				<div class="camp-thumb__wrap">
					<a href="<?php echo wp_get_attachment_image_src($gallery_id,'full')[0]?>" data-fancybox="images" class="camp-thumb__inner">
						<img data-src="<?php echo $image?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php echo get_the_title()?>" class="lazyload camp-thumb__img">
					</a>
				</div>
			</div>
		<?php endif?>

		<?php echo ($i > 4 && $count === $i) ? '</div>' : '';?>

	<?php endforeach?>
</div>
<?php endif?>
