<?php
/**
 * Product sale benefit
 *
 * @author WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php if ($product->is_on_sale() && $product->product_type == 'variable') : ?>

	<div class="benefit">
		<div class="inside">
			<div class="inside-text">
                <span>Скидка:</span>
				<?php
				$available_variations = $product->get_available_variations();
				$maximumper = 0;
				for ($i = 0; $i < count($available_variations); ++$i) {
					$variation_id=$available_variations[$i]['variation_id'];
					$variable_product1= new WC_Product_Variation( $variation_id );
					$regular_price = $variable_product1 ->regular_price;
					$sales_price = $variable_product1 ->sale_price;
					$benefit= round(($regular_price - $sales_price),1) ;
					if ($benefit > $maximumper) {
						$maximumper = $benefit;
					}
				}
				echo $price . sprintf( __('%s', 'woocommerce' ), $maximumper . 'р.' ); ?></div>
		</div>
	</div><!-- end callout -->

<?php elseif($product->is_on_sale() && $product->product_type == 'simple') : ?>

	<div class="benefit">
		<div class="inside">
			<div class="inside-text">
				<span>Скидка:</span>
				<?php
				$benefit = round( $product->regular_price - $product->sale_price );
				echo $price . sprintf( __('%s', 'woocommerce' ), $benefit . 'р.' ); ?></div>
		</div>
	</div><!-- end bubble -->

<?php endif; ?>