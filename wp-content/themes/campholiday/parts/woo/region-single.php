<?php
	global $product;
	$cats = get_terms_post($product->get_id(),'product_cat')
?>
<?php if(is_for($cats)):?>
	<tr class="camp-desc-info__row">
		<td class="camp-desc-info__title">
			<span>Регион</span>
		</td>
		<td class="camp-desc-info__text">
			<span>
				<?php $count = count($cats)?>
				<?php $i=0; foreach($cats as $cat): $i++?>
					<?php $suffix = ($count === $i) ? '' : ',';?>
					<?php echo $cat->name.$suffix?>
				<?php endforeach?>
			</span>
		</td>
	</tr>
<?php endif?>
