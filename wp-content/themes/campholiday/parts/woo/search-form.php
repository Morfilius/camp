<div class="search__wrap">
	<span class="search__title">Поиск</span>
	<form role="search" method="get" class="search" id="search" action="<?php echo esc_url(home_url('/')); ?>">
		<input type="hidden" name="post_type" value="product">
		<div class="search__select-wrap">
			<?php echo get_dropdown_term_parent('camp_type','category','Категория','category-js')?>
		</div>
		<div class="search__select-wrap">
			<select data-placeholder="Тип категории" name="type" class="search__select select-js type-js disable">
				<option></option>
			</select>
		</div>

		<div class="search__select-wrap">
			<?php echo get_dropdown_term('pa_strana','strana','Страна')?>
		</div>

		<div class="search__select-wrap">
			<?php echo get_dropdown_term('pa_region','region','Регион')?>
		</div>

		<div class="search__select-wrap">
			<?php echo get_dropdown_term('pa_sezon','sezon','Сезон')?>

		</div>

		<input type="text" name="s" class="search__item" placeholder="Введите название">
		<div class="tac">
			<button class="button search__button"><span>Поиск</span></button>
		</div>

	</form>
</div>