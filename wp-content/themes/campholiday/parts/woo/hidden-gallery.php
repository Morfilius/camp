<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;
global $product;

$attachment_ids = $product->get_gallery_image_ids();?>

<?php if(is_for($attachment_ids)):?>
	<div class="container-camps-top">
		<div id='camps-top' class="article-items">
			<?php foreach($attachment_ids as $gallery_id):?>
				<?php if($image = wp_get_attachment_image_src($gallery_id,'article-item__img')[0]):?>
					<div class="article-item">
						<div class="article-item__inner">
							<div class="article-item__img-wrap">
								<a href="#" class="article-item__img-inner grd-blue">
									<img data-lazy="<?php echo $image?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="alt" class="article-item__img">
								</a>
							</div>
						</div>
					</div>
				<?php endif?>

			<?php endforeach?>
		</div>
		<div class="slider-arrows news-arrows" id="news-arrows"></div>
	</div>
<?php endif?>

