<?php
	global $product;
	$cats = get_terms_post($product->get_id(),'product_cat')
?>
<?php if(is_for($cats)):?>

				<?php $count = count($cats)?>
				<?php $i=0; foreach($cats as $cat): $i++?>
					<?php $suffix = ($count === $i) ? '' : ',';?>
					<?php echo $cat->name.$suffix?>
				<?php endforeach?>
<?php endif?>
