<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

global $product;

$post_thumbnail_id = $product->get_image_id();

if(!$post_thumbnail_id) return '';

$image = wp_get_attachment_image( $post_thumbnail_id, 'big-prod-thumb', false, array(
	'title'                   => get_post_field( 'post_title', $attachment_id ),
	'alt'                     => get_post_field( 'post_title', $attachment_id ),
	'class'                   => 'camp-slider-item__img',
) );

$image_full = wp_get_attachment_image_src($post_thumbnail_id,'full')[0];

$html = '<div class="camp-slider-item">
            <div class="camp-slider-item__wrap">
                <div class="camp-slider-item__inner">
                    '.$image.'
                 </div>
            </div>
        </div>';
?>




<? echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );