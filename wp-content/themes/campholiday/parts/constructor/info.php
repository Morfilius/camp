<?php
	global $product;
?>
<div class="placing">
	<?php if($photos = get_sub_field('photo')):?>
		<?php foreach($photos as $photo):?>
			<div class="placing__img-item">
				<img src="<?php echo $photo['sizes']['prod-info']?>" alt="<?php the_title()?>" class="placing__img">
			</div>
		<?php endforeach?>
	<?php endif?>

	<div class="placing__text-item">
		<?php if(get_field('text_1')):?>
			<span class="text-row text-row__star">
	            <span class="text-row__text-bold">Программа:</span>
	             <?php the_field('text_1')?>
	        </span>
		<?php endif?>

		<?php if(get_field('text_2')):?>
			<span class="text-row text-row__bed">
	            <span class="text-row__text-bold">Проживание:</span>
				<?php the_field('text_2')?>
	        </span>
		<?php endif?>

		<?php if(get_field('text_3')):?>
			<span class="text-row text-row__shower">
		        <span class="text-row__text-bold">Удобства:</span>
		            <?php the_field('text_3')?>
	        </span>
		<?php endif?>

		<?php if ( $price_html = $product->get_price_html() ) : ?>
			<span class="text-row text-row__price">
	        <span class="text-row__text-bold">Стоимость:</span>
	             <?php echo $price_html?>
	        </span>
		<?php endif; ?>

	</div>
</div>
