<?php if( have_rows('constructor') ):?>

	<?php while ( have_rows('constructor') ) : the_row(); ?>

		<?php if( get_row_layout() == 'text_block' ):?>

			<?php get_template_part('parts/constructor/text')?>

		<?php elseif( get_row_layout() == 'adv_block' ):?>

			<?php get_template_part('parts/constructor/adv')?>

		<?php elseif( get_row_layout() == 'camps_block' ):?>

			<?php get_template_part('parts/constructor/camps')?>

		<?php elseif( get_row_layout() == 'info_block' ):?>

			<?php get_template_part('parts/constructor/info')?>

		<?php endif;?>

	<?php endwhile;?>

<?php else :?>

<?php endif;