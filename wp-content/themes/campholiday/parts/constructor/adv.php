<?php

$adv = get_sub_field('adv');
$info = get_sub_field('info');

?>

<?php if(is_for($adv) || $adv):?>
	<div class="camp-info-block">


		<?php if( have_rows('adv') ):?>
			<ol class="ol">
		    <?php while ( have_rows('adv') ) : the_row();?>
				<li class="ol__item">
		        <?php the_sub_field('text');?>
				</li>
		    <?php endwhile;?>
			</ol>
		<?php endif;?>


		<?php if($info):?>
			<div class="camp-info">
				<div class="camp-info__text">
					<?php echo do_shortcode($info)?>
				</div>
			</div>
		<?php endif?>

	</div>
<?php endif?>

