<?php
	$camps = get_sub_field('camps');
	$squads = get_sub_field('squad');
?>

<?php if(is_for($camps) || is_for($squads)):?>
	<div class="camp-center">
		<div class="row">
			<?php if(is_for($camps)):?>
				<div class="<?php echo is_for($squads) ? 'col-lg-9' : 'col-lg-12';?>">
					<div class="row">
						<?php foreach($camps as $post):?>
							<?php setup_postdata($post); global $product;?>
							<div class="col-md-6 mb14">
								<div class="camp-center-item">
									<header class="camp-center-item__top">
										<span <?php echo get_field('logo_camp')['url'] ? 'style="background-image: url('.get_field('logo_camp')['url'].')"' : '';?> class="camp-center-item__logo"></span>
										<a href="<?php the_permalink()?>" class="camp-center-item__title"><?php the_title()?></a>
									</header>
									<div class="camp-center-item__content">
										<div class="camp-center-item__img-container">
											<div class="camp-center-item__img-wrap">
												<div class="camp-center-item__img-inner">
													<?php the_post_thumbnail( 'prod-camp', array(
														'class' => 'camp-center-item__img',
														'alt'   => get_the_title()
													) ); ?>
												</div>
											</div>
										</div>
										<div class="camp-center-item__rows">
											<?php if(get_field('text_1')):?>
												<span class="text-row text-row__star-b camp-center-item__row">
			                                        <span class="text-row__text-bold">Программа:</span>
			                                        <?php the_field('text_1')?>
			                                    </span>
											<?php endif?>

											<?php if(get_field('text_2')):?>
												<span class="text-row text-row__bed-b camp-center-item__row">
				                                    <span class="text-row__text-bold">Проживание:</span>
				                                        <?php the_field('text_2')?>
			                                    </span>
											<?php endif?>

											<?php if(get_field('text_3')):?>
												<span class="text-row text-row__shower-b camp-center-item__row">
				                                    <span class="text-row__text-bold">Удобства:</span>
				                                        <?php the_field('text_3')?>
			                                    </span>
											<?php endif?>

											<?php if ( $price_html = $product->get_price_html() ) : ?>
                                                <span class="text-row text-row__price-b camp-center-item__row">
                                                    <span class="text-row__text-bold">Стоимость:</span>
                                                    <?php echo $price_html?>
                                                </span>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach?>
					</div>
				</div>
				<?php wp_reset_postdata()?>
			<?php endif?>

            <?php if(is_for($squads)):?>
                <div class="col-lg-3 pb14">
                    <div class="thematic-squad">
                        <div class="thematic-squad__top">
                            <span class="thematic-squad__title">Тематические отряды:</span>
                        </div>
                        <?php foreach($squads as $post):?>
                            <?php setup_postdata($post); global $product?>
                            <div class="thematic-squad__item">
                                <span class="thematic-squad__logo" <?php echo get_field('logo_squad')['url'] ? 'style="background-image: url('.get_field('logo_squad')['url'].')"' : '';?>></span>
                                <a href="<?php the_permalink()?>" class="thematic-squad__item-title"><?php the_title()?></a>
	                            <?php if ( $price_html = $product->get_price_html() ) : ?>
                                    <p class="thematic-squad__text">
                                        Стоимость:<br>
                                        <?php echo $price_html?>
                                    </p>
	                            <?php endif; ?>
                            </div>
                        <?php endforeach?>

                    </div>
                </div>
	            <?php wp_reset_postdata()?>
            <?php endif?>

		</div>
	</div>
<?php endif?>

