
<div class="home-sliders-wrap">

    <?php if( have_rows('slider') ):?>
    <div class="home-slider">
        <?php while ( have_rows('slider') ) : the_row();?>

            <div class="home-slider__item">
                <?php if($link = get_sub_field('link')):?>
                    <a href="<?php echo $link?>" class="home-slider__title"><?php the_sub_field('title')?></a>
                    <a href="<?php echo $link?>">
                        <img data-src="<?php echo get_sub_field('image')['sizes']['home-slider']?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_sub_field('title')?>" class="lazyload home-slider__img">
                    </a>
                <?php else:?>
                    <span class="home-slider__title"><?php the_sub_field('title')?></span>
                    <img data-src="<?php echo get_sub_field('image')['sizes']['home-slider']?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_sub_field('title')?>" class="lazyload home-slider__img">
                <?php endif?>
            </div>


        <?php endwhile;?>
    </div>
    <?php endif;?>

    <?php if( have_rows('slider') ):?>
    <div class="home-slider-thumb-wrap">
        <div class="home-slider-thumb">
        <?php while ( have_rows('slider') ) : the_row();?>
            <div class="home-slider-thumb__item">
                <span class="home-slider-thumb__title"><?php the_sub_field('title')?></span>
                <img data-src="<?php echo get_sub_field('image')['sizes']['home-slider']?>" src="<?php bloginfo('template_url'); ?>/img/no-image.png" alt="<?php the_sub_field('title')?>" class="lazyload home-slider-thumb__img">
            </div>
        <?php endwhile;?>
        </div>
    </div>
    <?php endif;?>
</div>