<div class="related">
	<div class="container container-news container-related">
		<div class="related__inner ">
			<div id='articles' class="article-items">
				<?php $related = get_related_posts()?>
				<?php if(!empty($related) && $related->have_posts()):?>
					<?php while($related->have_posts()):$related->the_post()?>
						<div class="article-item related-item">
							<div class="article-item__inner">
								<div class="article-item__img-wrap related-item__img-wrap">
									<a href="<?php the_permalink()?>" class="article-item__img-inner">
										<?php $img = get_the_post_thumbnail_url($post->ID,'related-thumb')?>
										<img data-lazy="<?php echo $img?>" src="<?php bloginfo('template_url'); ?>/img/placeholder.png" alt="<?php the_title()?>" class="article-item__img">
									</a>
								</div>
								<footer class="article-item__bottom">
									<a href='<?php the_permalink()?>' class="article-item__title related__title">
										<?php the_title()?>
									</a>
								</footer>
							</div>
						</div>
					<?php endwhile;?>
				<?php endif?>
			</div>
			<div class="slider-arrows news-arrows" id="news-arrows"></div>
		</div>
	</div>