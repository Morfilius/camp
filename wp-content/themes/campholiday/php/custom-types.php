<?php


if ( ! function_exists( 'camp_type' ) ) {
	function camp_type() {
		$labels = array(
			'name'              => 'Тип товара',
			'singular_name'     => 'Тип товара',
			'search_items'      => 'Найти тип товара',
			'all_items'         => 'Все тип товара',
			'edit_item'         => 'Редактировать тип товара',
			'update_item'       => 'Обновить тип товара',
			'add_new_item'      => 'Добавить новый тип товара',
			'new_item_name'     => 'Новое название типа товара',
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'camp_type', 'product', $args );
	}
	add_action( 'init', 'camp_type', 0 ); // инициализируем
}




