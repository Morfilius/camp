<?php
class Links_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'links_widget', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: links_widget
			'Блок ссылок',
			array( 'description' => 'Ссылки на партнеров', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		echo $args['before_widget'];
		$links = get_field('links', 'widget_' . $args['widget_id'])
		?>
        <?php if(is_for($links)):?>
            <?php foreach($links as $link):?>
                <a href="<?php echo $link['link']?>" class="footer-link">
                    <img src="<?php echo $link['img']['url']?>" alt="alt" class="footer-link__img">
                </a>
            <?php endforeach?>
        <?php endif?>
		<?
		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {
		$title = @ $instance['title'] ?: 'Заголовок по умолчанию';

		?>
        <p></p>
		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		$instance = array();
		return $instance;
	}


}
// конец класса Links_Widget

// регистрация Links_Widget в WordPress
function register_links_widget() {
	register_widget( 'Links_Widget' );
}
add_action( 'widgets_init', 'register_links_widget' );