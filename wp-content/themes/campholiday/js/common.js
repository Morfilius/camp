!function ($) {
    //qty price
    $(document).ready(function () {
        !function () {
            $('.counter__input').on('change',function () {
                var el  = $('.booking .woocommerce-Price-amount');
                var qty = $(this).val();
                el.each(function () {
                    var match = $(this).text().match(/([0-9]+\.*[0-9]+)(.*)/);


                    if(!!match && match.length ===  3){
                        var price = +match[1];
                        var cur   = match[2];

                        var data_price = this.dataset.price;
                        if(!data_price) {
                            this.dataset.price = price;
                        }else{
                            price = data_price;
                        }

                        $(this).html((price*qty).toFixed(2)+'<span class="woocommerce-Price-currencySymbol">'+cur+'</span>');
                    }
                });

            });

            $( ".variations_form" ).on( "woocommerce_variation_select_change", function () {
                $('.counter__input').val(1);
            } );
        }();
    });

    !function () {
        $('.category-js').on('change',function () {
            var type      = $('.type-js');
            type.find('option').remove();
            type.addClass('disable');

            var data = {
                action : 'select',
                category : 'type',
                value: $(this).val()
            };

            $.post( myajax, data, function( response ) {
                var options   = JSON.parse(response);

                if(!!options.result && options.result === 'none') return;
                type.append('<option></option>');
                for(var i=0; i < options.length; i++){
                    var newOption = new Option( options[i].name,  options[i].value, false, false);
                    type.append(newOption).trigger('change');
                }
                setTimeout(function () {
                    type.val(null).trigger('change');
                },100);
                type.removeClass('disable');
            });

        })
    }();

    !function () {
        var form = $( "#search" );
        form.submit(function() {
            $(this).find(":input").filter(function(){ return (!this.value); }).attr("disabled", "disabled");
            return true;
        });

        form.find( ":input" ).prop( "disabled", false );
    }();

    !function () {
        $('.post-gallery').slick({
            dots: true,
            customPaging : function(slider, i) {
                return '<span></span>';
            },
            infinite: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
            fade: true,
            cssEase: 'linear'
        });
    }()

}(jQuery);