<?php
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>
    <main class="content">
        <div class="container">
		    <?php breadcrumbs()?>
        </div>
        <section class="contacts">
            <div class="container">
                <?php if (have_posts()) : ?>
                    <?while (have_posts()) : the_post();?>
                        <h1 class="title"><?php the_title()?></h1>
                        <?php the_content()?>
                        <?php get_template_part('parts/page-constructor/index')?>
                    <?php endwhile;?>
                <?else: echo '<p>Нет записей.</p>';?>
                <? endif;?>
            </div>
        </section>
    </main>
<?php get_footer(); // подключаем footer.php ?>