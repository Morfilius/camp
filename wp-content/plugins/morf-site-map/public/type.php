<div class="m-list-wrap">
	<a href="<?php echo $data['link']?>" class="m-list-wrap__title"><?php echo $data['title']?></a>
    <?php if(is_array($data['content'])):?>
    <ul class="m-list">
    <?php foreach($data['content'] as $content):?>
        <li>
            <a href="<?php echo $content['link']?>" class="m-list__link"><?php echo $content['title']?></a>
        </li>
    <?php endforeach?>
    </ul>
    <?php endif?>
</div>
