<?php

class Create_Shortcode {


	public function run(){
		add_shortcode( 'html-map', array($this,'echo_type_html'));
	}

	private function get_posts($tag){
		return get_posts(array(
			'numberposts' => -1,
			'post_type'   => $tag,
			'post_status' => 'publish'
		));
	}

	private function get_page_list($atts){
		unset($atts['type']);
		$args = array(
			'post_type' => 'page',
			'echo'      => false,
			'title_li'  => false,
		);
		$args = array_merge($args,$atts);
		return wp_list_pages($args);
	}

	private function prepare_data($atts){
		$data = array();
		$atts = shortcode_atts( array(
			'type' => 'page',
			'include' => '',
			'exclude' => ''
		), $atts );

		$tag = $atts['type'];

		if($tag === 'page'){
			$data = $this->get_page_list($atts);
		}
		else{
			$data['link'] = get_post_type_archive_link($tag);
			$data['title'] = get_post_type_object($tag)->labels->singular_name;

			$posts = $this->get_posts($tag);
			foreach ($posts as $post){
				$data['content'][] = array(
					'link'  => get_the_permalink($post->ID),
					'title' => $post->post_title
				);
			}
		}
		if(empty($data)) return false;

		return $data;
	}

	public function echo_type_html($atts){
		$data = $this->prepare_data($atts);
		if($atts['type'] === 'page'){
			if($data){
				if (is_file(__DIR__.'/../public/page.php')) {
					ob_start();
					require (__DIR__.'/../public/page.php');
					return ob_get_clean();
				}
			}
		}else{
			if(is_array($data)){
				if (is_file(__DIR__.'/../public/type.php')) {
					ob_start();
					require (__DIR__.'/../public/type.php');
					return ob_get_clean();
				}
			}
		}

		return false;
	}
}